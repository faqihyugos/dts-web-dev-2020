<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Produk</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body>
    <div class="container">
        <h1>List Produk</h1>
        <a href="create.php" class="btn btn-primary mb-2"> Tambah Produk</a>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">No</th>
                <th scope="col">Merek</th>
                <th scope="col">Warna</th>
                <th scope="col">Stok</th>
                <th scope="col">Satuan</th>
                <th scope="col">Harga</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <?php
                include "koneksi.php";

                // START get data from table produk
                $sql = "SELECT * from produk ORDER BY merek";
                // END get data from table produk

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                foreach ($hasil as $key => $data) {
                    ?>
                    <!-- START isi data -->
                    <tbody>
                        <tr>
                            <td><?php echo $key + 1 ?></td>
                            <td><?php echo $data['merek'] ?></td>
                            <td><?php echo $data['warna']?></td>
                            <td><?php echo $data['stok']?></td>
                            <td><?php echo $data['satuan']?></td>
                            <td><?php echo $data['harga']?></td>
                            <td>
                                <a href="edit.php?id=<?php echo $data['id']?>" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                                <a href="<?php echo $_SERVER['PHP_SELF']?>?id=<?php echo $data['id']?>" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                            </td>
                        </tr>
                    </tbody>
                    <!-- END isi data -->
                <?php
                }
                // END mengeksekusi data

                // CEK apakah ada method get di file ini
                if (isset($_GET['id'])) {
                    # code...
                    $id = $_GET['id'];
                    $sql = "DELETE FROM produk WHERE id=$id";
                    $hasil = mysqli_query($db, $sql);

                    // kondisi berhasil atau tidak
                    if ($hasil) {
                        header("Location:index.php");
                    } else {
                        echo "<div class='alert alert-danger'> Data gagal dihapus. </div>";
                    }
                }
                ?>
        </table>
    </div>
</body>
</html>