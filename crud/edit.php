<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Produk</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body>
    <div class="container">
        <?php
            include "koneksi.php";

            // START cek apakah ada kiriman form dari method POST
            // function input($data) {
            //     $data = trim($data);
            //     $data = stripcslashes($data);
            //     $data = htmlspecialchars($data);
            //     return $data;
            // }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $id     = $_POST["id"];
                $merek  = $_POST["merek"];
                $warna  = $_POST["warna"];
                $stok   = $_POST["stok"];
                $satuan = $_POST["satuan"];
                $harga  = $_POST["harga"];

                $sql = "UPDATE produk SET merek='$merek',warna='$warna',stok='$stok',satuan='$satuan',harga='$harga' WHERE id=$id"; 

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                // END mengeksekusi data

                // START cek hasil eksekusi
                if ($hasil) {
                    header("Location:index.php");
                } else {
                    echo "<div class='alert alert-danger'> Data gagal diubah. </div>";
                }
                // END cek hasil eksekusi
            }
            // END cek apakah ada kiriman form dari method POST

            // START get data edit
            if (isset($_GET['id'])) {
                $id = $_GET['id'];

                $sql = "SELECT * FROM produk where id = $id";
                $hasil = mysqli_query($db, $sql);
                $data = mysqli_fetch_assoc($hasil);
            }
            // END get data edit

        ?>

        <h5>Ubah Produk</h5>
        <form action="<?php echo($_SERVER['PHP_SELF']) ?>" method="post" id="form">
            <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
            <div class="form-group">
                <label for="merek">Merek</label>
                <input type="text" name="merek" placeholder="Masukkan merek" class="form-control" id="" aria-describedby="emailHelp" required value="<?php echo $data['merek'] ?>">
            </div>
            <div class="form-group">
                <label for="warna">Warna</label>
                <input type="text" name="warna" placeholder="Masukkan warna" class="form-control" id="" required value="<?php echo $data['warna'] ?>">
            </div>
            <div class="form-group">
                <label for="stok">Stok</label>
                <input type="number" name="stok" placeholder="Masukkan jumlah stok tersedia" class="form-control" id="" required value="<?php echo $data['stok'] ?>">
            </div>
            <div class="form-group">
                <label for="satuan">Satuan</label>
                <input type="number" name="satuan" placeholder="Masukkan satuan produk" class="form-control" id="" required value="<?php echo $data['satuan'] ?>">
            </div>
            <div class="form-group">
                <label for="harga">Harga</label>
                <input type="number" name="harga" placeholder="Masukkan harga hanya angka" class="form-control" id="" required value="<?php echo $data['harga'] ?>">
            </div>
            <a href="index.php" class="btn btn-warning"> Kembali</a>
            <button type="button" id="btn-ulangi" class="btn btn-danger">Reset</button>
            <button type="submit" placeholder="Masukkan merek" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</body>
</html>